﻿namespace Euro_Option_Simulator
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox_Underlying = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RadioButton_Call = new System.Windows.Forms.RadioButton();
            this.Button_Calculate = new System.Windows.Forms.Button();
            this.TextBox_Strike = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_Rate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBox_Volatility = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBox_Tenor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBox_Price = new System.Windows.Forms.TextBox();
            this.TextBox_Delta = new System.Windows.Forms.TextBox();
            this.TextBox_Gamma = new System.Windows.Forms.TextBox();
            this.TextBox_Vega = new System.Windows.Forms.TextBox();
            this.TextBox_Theta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TextBox_Rho = new System.Windows.Forms.TextBox();
            this.TextBox_Error = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.RadioButton_Put = new System.Windows.Forms.RadioButton();
            this.TextBox_Step = new System.Windows.Forms.TextBox();
            this.TextBox_Trial = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RadioButton_Use_antithetic = new System.Windows.Forms.RadioButton();
            this.RadioButton_Not_use_antithetic = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadioButton_Use_delta = new System.Windows.Forms.RadioButton();
            this.RadioButton_Not_use_delta = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Timer = new System.Windows.Forms.Label();
            this.Time = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBox_Underlying
            // 
            this.TextBox_Underlying.Location = new System.Drawing.Point(154, 82);
            this.TextBox_Underlying.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Underlying.Name = "TextBox_Underlying";
            this.TextBox_Underlying.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Underlying.TabIndex = 0;
            this.TextBox_Underlying.Text = "50\r\n";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 85);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Underlying";
            // 
            // RadioButton_Call
            // 
            this.RadioButton_Call.AutoSize = true;
            this.RadioButton_Call.Checked = true;
            this.RadioButton_Call.Location = new System.Drawing.Point(15, 12);
            this.RadioButton_Call.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RadioButton_Call.Name = "RadioButton_Call";
            this.RadioButton_Call.Size = new System.Drawing.Size(47, 16);
            this.RadioButton_Call.TabIndex = 2;
            this.RadioButton_Call.TabStop = true;
            this.RadioButton_Call.Text = "Call";
            this.RadioButton_Call.UseVisualStyleBackColor = true;
            // 
            // Button_Calculate
            // 
            this.Button_Calculate.Location = new System.Drawing.Point(592, 139);
            this.Button_Calculate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Button_Calculate.Name = "Button_Calculate";
            this.Button_Calculate.Size = new System.Drawing.Size(128, 83);
            this.Button_Calculate.TabIndex = 3;
            this.Button_Calculate.Text = "Calculate";
            this.Button_Calculate.UseVisualStyleBackColor = true;
            this.Button_Calculate.Click += new System.EventHandler(this.button1_Click);
            // 
            // TextBox_Strike
            // 
            this.TextBox_Strike.Location = new System.Drawing.Point(154, 117);
            this.TextBox_Strike.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Strike.Name = "TextBox_Strike";
            this.TextBox_Strike.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Strike.TabIndex = 4;
            this.TextBox_Strike.Text = "50";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "Strike price";
            // 
            // TextBox_Rate
            // 
            this.TextBox_Rate.Location = new System.Drawing.Point(154, 147);
            this.TextBox_Rate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Rate.Name = "TextBox_Rate";
            this.TextBox_Rate.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Rate.TabIndex = 6;
            this.TextBox_Rate.Text = "0.05";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 150);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Risk-free rate";
            // 
            // TextBox_Volatility
            // 
            this.TextBox_Volatility.Location = new System.Drawing.Point(154, 177);
            this.TextBox_Volatility.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Volatility.Name = "TextBox_Volatility";
            this.TextBox_Volatility.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Volatility.TabIndex = 8;
            this.TextBox_Volatility.Text = "0.5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 180);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "Volatility";
            // 
            // TextBox_Tenor
            // 
            this.TextBox_Tenor.Location = new System.Drawing.Point(154, 207);
            this.TextBox_Tenor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Tenor.Name = "TextBox_Tenor";
            this.TextBox_Tenor.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Tenor.TabIndex = 10;
            this.TextBox_Tenor.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 210);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "Tenor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(163, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(451, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Monte Carlo Simulation with delta-based control variate";
            // 
            // TextBox_Price
            // 
            this.TextBox_Price.Location = new System.Drawing.Point(399, 87);
            this.TextBox_Price.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Price.Name = "TextBox_Price";
            this.TextBox_Price.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Price.TabIndex = 13;
            // 
            // TextBox_Delta
            // 
            this.TextBox_Delta.Location = new System.Drawing.Point(399, 117);
            this.TextBox_Delta.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Delta.Name = "TextBox_Delta";
            this.TextBox_Delta.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Delta.TabIndex = 14;
            // 
            // TextBox_Gamma
            // 
            this.TextBox_Gamma.Location = new System.Drawing.Point(399, 147);
            this.TextBox_Gamma.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Gamma.Name = "TextBox_Gamma";
            this.TextBox_Gamma.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Gamma.TabIndex = 15;
            // 
            // TextBox_Vega
            // 
            this.TextBox_Vega.Location = new System.Drawing.Point(399, 177);
            this.TextBox_Vega.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Vega.Name = "TextBox_Vega";
            this.TextBox_Vega.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Vega.TabIndex = 16;
            // 
            // TextBox_Theta
            // 
            this.TextBox_Theta.Location = new System.Drawing.Point(399, 207);
            this.TextBox_Theta.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Theta.Name = "TextBox_Theta";
            this.TextBox_Theta.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Theta.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 90);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 18;
            this.label7.Text = "Price";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(283, 120);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 19;
            this.label8.Text = "Delta";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(283, 150);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 20;
            this.label9.Text = "Gamma";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(283, 180);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "Vega";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(283, 210);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 12);
            this.label11.TabIndex = 22;
            this.label11.Text = "Theta";
            // 
            // TextBox_Rho
            // 
            this.TextBox_Rho.Location = new System.Drawing.Point(399, 238);
            this.TextBox_Rho.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Rho.Name = "TextBox_Rho";
            this.TextBox_Rho.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Rho.TabIndex = 23;
            // 
            // TextBox_Error
            // 
            this.TextBox_Error.Location = new System.Drawing.Point(399, 267);
            this.TextBox_Error.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Error.Name = "TextBox_Error";
            this.TextBox_Error.Size = new System.Drawing.Size(118, 21);
            this.TextBox_Error.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(283, 241);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 12);
            this.label12.TabIndex = 25;
            this.label12.Text = "Rho";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(283, 270);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 26;
            this.label13.Text = "Standard error";
            // 
            // RadioButton_Put
            // 
            this.RadioButton_Put.AutoSize = true;
            this.RadioButton_Put.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RadioButton_Put.Location = new System.Drawing.Point(16, 40);
            this.RadioButton_Put.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RadioButton_Put.Name = "RadioButton_Put";
            this.RadioButton_Put.Size = new System.Drawing.Size(41, 16);
            this.RadioButton_Put.TabIndex = 27;
            this.RadioButton_Put.Text = "Put";
            this.RadioButton_Put.UseVisualStyleBackColor = true;
            // 
            // TextBox_Step
            // 
            this.TextBox_Step.Location = new System.Drawing.Point(154, 238);
            this.TextBox_Step.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Step.Name = "TextBox_Step";
            this.TextBox_Step.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Step.TabIndex = 28;
            this.TextBox_Step.Text = "100";
            // 
            // TextBox_Trial
            // 
            this.TextBox_Trial.Location = new System.Drawing.Point(154, 267);
            this.TextBox_Trial.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBox_Trial.Name = "TextBox_Trial";
            this.TextBox_Trial.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Trial.TabIndex = 29;
            this.TextBox_Trial.Text = "10000";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(60, 241);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 30;
            this.label14.Text = "Step";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(60, 270);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 12);
            this.label15.TabIndex = 31;
            this.label15.Text = "Trial";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RadioButton_Use_antithetic);
            this.panel2.Controls.Add(this.RadioButton_Not_use_antithetic);
            this.panel2.Location = new System.Drawing.Point(22, 305);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 75);
            this.panel2.TabIndex = 32;
            // 
            // RadioButton_Use_antithetic
            // 
            this.RadioButton_Use_antithetic.AutoSize = true;
            this.RadioButton_Use_antithetic.Checked = true;
            this.RadioButton_Use_antithetic.Location = new System.Drawing.Point(2, 13);
            this.RadioButton_Use_antithetic.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RadioButton_Use_antithetic.Name = "RadioButton_Use_antithetic";
            this.RadioButton_Use_antithetic.Size = new System.Drawing.Size(221, 16);
            this.RadioButton_Use_antithetic.TabIndex = 32;
            this.RadioButton_Use_antithetic.TabStop = true;
            this.RadioButton_Use_antithetic.Text = "Use antithetic variance reduction";
            this.RadioButton_Use_antithetic.UseVisualStyleBackColor = true;
            // 
            // RadioButton_Not_use_antithetic
            // 
            this.RadioButton_Not_use_antithetic.AutoSize = true;
            this.RadioButton_Not_use_antithetic.Location = new System.Drawing.Point(2, 40);
            this.RadioButton_Not_use_antithetic.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RadioButton_Not_use_antithetic.Name = "RadioButton_Not_use_antithetic";
            this.RadioButton_Not_use_antithetic.Size = new System.Drawing.Size(245, 16);
            this.RadioButton_Not_use_antithetic.TabIndex = 33;
            this.RadioButton_Not_use_antithetic.Text = "Not use antithetic variance reduction";
            this.RadioButton_Not_use_antithetic.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadioButton_Call);
            this.panel1.Controls.Add(this.RadioButton_Put);
            this.panel1.Location = new System.Drawing.Point(622, 305);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(88, 75);
            this.panel1.TabIndex = 33;
            // 
            // RadioButton_Use_delta
            // 
            this.RadioButton_Use_delta.AutoSize = true;
            this.RadioButton_Use_delta.Checked = true;
            this.RadioButton_Use_delta.Location = new System.Drawing.Point(12, 12);
            this.RadioButton_Use_delta.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RadioButton_Use_delta.Name = "RadioButton_Use_delta";
            this.RadioButton_Use_delta.Size = new System.Drawing.Size(209, 16);
            this.RadioButton_Use_delta.TabIndex = 34;
            this.RadioButton_Use_delta.TabStop = true;
            this.RadioButton_Use_delta.Text = "Use delta-based control variate";
            this.RadioButton_Use_delta.UseVisualStyleBackColor = true;
            // 
            // RadioButton_Not_use_delta
            // 
            this.RadioButton_Not_use_delta.AutoSize = true;
            this.RadioButton_Not_use_delta.Location = new System.Drawing.Point(12, 42);
            this.RadioButton_Not_use_delta.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RadioButton_Not_use_delta.Name = "RadioButton_Not_use_delta";
            this.RadioButton_Not_use_delta.Size = new System.Drawing.Size(233, 16);
            this.RadioButton_Not_use_delta.TabIndex = 35;
            this.RadioButton_Not_use_delta.Text = "Not use delta-based control variate";
            this.RadioButton_Not_use_delta.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.RadioButton_Use_delta);
            this.panel3.Controls.Add(this.RadioButton_Not_use_delta);
            this.panel3.Location = new System.Drawing.Point(317, 305);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(211, 75);
            this.panel3.TabIndex = 36;
            // 
            // Timer
            // 
            this.Timer.AutoSize = true;
            this.Timer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Timer.Location = new System.Drawing.Point(560, 78);
            this.Timer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Timer.Name = "Timer";
            this.Timer.Size = new System.Drawing.Size(70, 24);
            this.Timer.TabIndex = 37;
            this.Timer.Text = "Timer:";
            // 
            // Time
            // 
            this.Time.AutoSize = true;
            this.Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.Location = new System.Drawing.Point(634, 79);
            this.Time.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Time.Name = "Time";
            this.Time.Size = new System.Drawing.Size(116, 24);
            this.Time.TabIndex = 38;
            this.Time.Text = "00:00:00:00";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 449);
            this.Controls.Add(this.Time);
            this.Controls.Add(this.Timer);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TextBox_Trial);
            this.Controls.Add(this.TextBox_Step);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TextBox_Error);
            this.Controls.Add(this.TextBox_Rho);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TextBox_Theta);
            this.Controls.Add(this.TextBox_Vega);
            this.Controls.Add(this.TextBox_Gamma);
            this.Controls.Add(this.TextBox_Delta);
            this.Controls.Add(this.TextBox_Price);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TextBox_Tenor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBox_Volatility);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBox_Rate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBox_Strike);
            this.Controls.Add(this.Button_Calculate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBox_Underlying);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Monte Carlo Simulation";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox_Underlying;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton RadioButton_Call;
        private System.Windows.Forms.Button Button_Calculate;
        private System.Windows.Forms.TextBox TextBox_Strike;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBox_Rate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBox_Volatility;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBox_Tenor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBox_Price;
        private System.Windows.Forms.TextBox TextBox_Delta;
        private System.Windows.Forms.TextBox TextBox_Gamma;
        private System.Windows.Forms.TextBox TextBox_Vega;
        private System.Windows.Forms.TextBox TextBox_Theta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TextBox_Rho;
        private System.Windows.Forms.TextBox TextBox_Error;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton RadioButton_Put;
        private System.Windows.Forms.TextBox TextBox_Step;
        private System.Windows.Forms.TextBox TextBox_Trial;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton RadioButton_Use_antithetic;
        private System.Windows.Forms.RadioButton RadioButton_Not_use_antithetic;
        private System.Windows.Forms.RadioButton RadioButton_Use_delta;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton RadioButton_Not_use_delta;
        private System.Windows.Forms.Label Timer;
        private System.Windows.Forms.Label Time;
    }
}

