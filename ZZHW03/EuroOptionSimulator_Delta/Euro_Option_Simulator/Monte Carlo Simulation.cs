﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Euro_Option_Simulator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            try
            {

                //read inputs from textboxes
                double S0 = Convert.ToDouble(TextBox_Underlying.Text);
                double K = Convert.ToDouble(TextBox_Strike.Text);
                double r = Convert.ToDouble(TextBox_Rate.Text);
                double sigma = Convert.ToDouble(TextBox_Volatility.Text);
                double T = Convert.ToDouble(TextBox_Tenor.Text);
                int N = Convert.ToInt32(TextBox_Step.Text);
                int M = Convert.ToInt32(TextBox_Trial.Text);

                if (S0 > 0 && K > 0 && r > 0 && sigma > 0 && T > 0 && N > 1 && M > 0)
                {
                    Stopwatch watch = new Stopwatch();
                    //reset the timer
                    watch.Reset();
                    Time.Text = "00:00:00:00";
                    watch.Start();
                    //output results through form text
                    double[,] random = GetRandom(N,M);
                    TextBox_Price.Text = Convert.ToString(European(K, S0, r, T, sigma, N, M, random)[0]);
                    TextBox_Error.Text = Convert.ToString(European(K, S0, r, T, sigma, N, M, random)[1]);
                    TextBox_Delta.Text = Convert.ToString(Delta(K, S0, r, T, sigma, N, M, random));
                    TextBox_Gamma.Text = Convert.ToString(Gamma(K, S0, r, T, sigma, N, M, random));
                    TextBox_Vega.Text = Convert.ToString(Vega(K, S0, r, T, sigma, N, M, random));
                    TextBox_Theta.Text = Convert.ToString(Theta(K, S0, r, T, sigma, N, M, random));
                    TextBox_Rho.Text = Convert.ToString(Rho(K, S0, r, T, sigma, N, M, random));
                    watch.Stop();
                    Time.Text = watch.Elapsed.Hours.ToString() + ":" + watch.Elapsed.Minutes.ToString() + ":" + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString();
                }

                else
                {
                    MessageBox.Show("Please input positive number in every leftward textbox ans step should be greater than 1.");
                }
            }
            catch
            {
                MessageBox.Show("Please input positive number in every leftward textbox and step should be greater than 1.");
            }

        }

        //implement a CDF method
        private double CDF(double z)
        {
            double p = 0.3275911;
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;

            int sign;
            if (z < 0.0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(z) / Math.Sqrt(2.0);
            double t = 1.0 / (1.0 + p * x);
            double erf = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);
            double cdf = 0.5 * (1.0 + sign * erf);
            return cdf;
        }

        //implement Black Scholes model to calculate delta
        private double Black_Scholes(double K, double S0, double r, double T, double sigma)
        {
            //calculate some inputs of Black Sholes formular
            double nuT = (r + 0.5 * Math.Pow(sigma, 2)) * T;
            double sigsT = sigma * Math.Sqrt(T);
            double erdT = Math.Exp(r * T);
            double d1 = (Math.Log(S0 / K) + nuT) / sigsT;
            double d2 = d1 - sigsT;
            double option_price = 0;

            //calculate option price using Black Sholes formular
            if(RadioButton_Call.Checked)
            {
                option_price = S0 * CDF(d1) - K * erdT * CDF(d2);
            }
            else
            {
                option_price = K * erdT * CDF(-d2) - S0 * CDF(-d1);
            }
            return option_price;
        }

        //calculate delta using Black Scholes model
        private double BSdelta(double K, double S0, double r, double T, double sigma)
        {
            double BSunderlying_plus = Black_Scholes(K, S0 + 0.001 * S0, r, T, sigma);
            double BSunderlying_minus = Black_Scholes(K, S0 - 0.001 * S0, r, T, sigma);
            double bsdelta = (BSunderlying_plus - BSunderlying_minus) / (0.002 * S0);
            return bsdelta;
        }

        private double[,] GetRandom(int N, int M)
        {
            //set up a matrix to store random numbers
            double[,] random = new double[M, N + 1];
            Random rnd = new Random();
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j <= N; j++)
                //generate random numbers using polar rejection method
                {
                    double x1 = 0;
                    double x2 = 0;
                    double w = 2;
                    while (w > 1)
                    {
                        x1 = rnd.NextDouble() * 2 - 1;
                        x2 = rnd.NextDouble() * 2 - 1;
                        w = Math.Pow(x1, 2) + Math.Pow(x2, 2);
                    }
                    double c = 0;
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    random[i, j] = 0;
                    random[i, j] = c * x1;
                }
            }
            return random;
        } 

        private double[] European(double K, double S0, double r, double T, double sigma, int N, int M,double[,] random)
        {            
            //calculate some inputs of simulation formular
            double dt = T / N;
            double nudt = (r - 0.5 * Math.Pow(sigma, 2)) * dt;
            double sigsdt = sigma * Math.Sqrt(dt);
            double erddt = Math.Exp(r * dt);
            double betal = -1;

            double sum_price = 0;
            double sum_price_2 = 0;

            //when users choose to use both antithetic and delta - based control variate at the same time
            if (RadioButton_Use_antithetic.Checked && RadioButton_Use_delta.Checked)
            {
                //set up a matrix to store St
                double[,] S = new double[2 * M, N + 1];
                for (int i = 0; i < 2 * M; i++)
                {
                    S[i, 0] = S0;
                }

                //calculate St under trial i and step j
                for (int i = 0; i < M; i++)
                {
                    double cv1 = 0;
                    double cv2 = 0;
                    for (int j = 1; j <= N; j++)
                    {
                        double t = (j - 1) * dt;
                        double delta1 = BSdelta(K, S[i, j - 1], r, T-t, sigma);
                        double delta2 = BSdelta(K, S[i + M, j - 1], r, T-t, sigma);
                        double e = random[i, j];

                        S[i, j] = S[i, j - 1] * Math.Exp(nudt + sigsdt * e);
                        S[i + M, j] = S[i + M, j - 1] * Math.Exp(nudt + sigsdt * (-e));
                        cv1 = cv1 + delta1 * (S[i, j] - S[i, j - 1] * erddt);
                        cv2 = cv2 + delta2 * (S[i + M, j] - S[i + M, j - 1] * erddt);
                    }
                    //ditinguish call and put option,then calculate option price of each trial
                    double i_price = 0;
                    if (RadioButton_Call.Checked)
                    {
                        i_price = 0.5 * (Math.Max(0, S[i, N] - K) + Math.Max(0, S[i + M, N] - K) + betal * cv1 + betal * cv2);
                    }
                    else
                    {
                        i_price = 0.5 * (Math.Max(0, K - S[i, N]) + Math.Max(0, K - S[i + M, N]) + betal * cv1 + betal * cv2);
                    }
                    sum_price = sum_price + i_price;
                    sum_price_2 = sum_price_2 + Math.Pow(i_price, 2);
                }
            }

            //when users choose to only use antithetic variance reduction 
            else if (RadioButton_Use_antithetic.Checked && RadioButton_Not_use_delta.Checked)
            {
                //set up a matrix to store St
                double[,] S = new double[2 * M, N + 1];
                for (int i = 0; i < 2 * M; i++)
                {
                    S[i, 0] = S0;
                }

                //calculate St under trial i and step j
                for (int i = 0; i < M; i++)
                {
                    for (int j = 1; j <= N; j++)
                    {
                        double e = random[i, j];
                        S[i, j] = S[i, j - 1] * Math.Exp(nudt + sigma * Math.Sqrt(dt) * e);
                        S[i + M, j] = S[i + M, j - 1] * Math.Exp(nudt + sigma * Math.Sqrt(dt) * (-e));
                    }
                    //ditinguish call and put option,then calculate option price of each trial
                    double i_price = 0;
                    if (RadioButton_Call.Checked)
                    {
                        i_price = 0.5 * (Math.Max(0, S[i, N] - K) + Math.Max(0, S[i + M, N] - K));
                    }
                    else
                    {
                        i_price = 0.5 * (Math.Max(0, K - S[i, N]) + Math.Max(0, K - S[i + M, N]));
                    }
                    sum_price = sum_price + i_price;
                    sum_price_2 = sum_price_2 + Math.Pow(i_price, 2);
                }
            }

            //when users choose to only use delta-based control variate
            else if (RadioButton_Not_use_antithetic.Checked && RadioButton_Use_delta.Checked)
            {
                //set up a matrix to store St
                double[,] S = new double[M, N + 1];
                for (int i = 0; i < M; i++)
                {
                    S[i, 0] = S0;
                }

                //calculate St under trial i and step j
                for (int i = 0; i < M; i++)
                {
                    double cv = 0;
                    for (int j = 1; j <= N; j++)
                    {
                        double t = (j - 1) * dt;
                        double delta = BSdelta(K, S[i, j - 1], r, T-t, sigma);
                        double e = random[i, j];

                        S[i, j] = S[i, j - 1] * Math.Exp(nudt + sigsdt * e);
                        cv = cv + delta * (S[i, j] - S[i, j - 1] * erddt);
                    }
                    //ditinguish call and put option,then calculate option price of each trial
                    double i_price = 0;
                    if (RadioButton_Call.Checked)
                    {
                        i_price = Math.Max(0, S[i, N] - K) + betal * cv;
                    }
                    else
                    {
                        i_price = Math.Max(0, K - S[i, N]) + betal * cv;
                    }
                    sum_price = sum_price + i_price;
                    sum_price_2 = sum_price_2 + Math.Pow(i_price, 2);
                }
            }

            //when users choose not to use either antithetic or delta - based control variate
            else
            {
                //set up a matrix to store St
                double[,] S = new double[M, N + 1];
                for (int i = 0; i < M; i++)
                {
                    S[i, 0] = S0;
                }

                //calculate St under trial i and step j
                for (int i = 0; i < M; i++)
                {
                    for (int j = 1; j <= N; j++)
                    {
                        double e = random[i, j];
                        S[i, j] = S[i, j - 1] * Math.Exp(nudt + sigma * Math.Sqrt(dt) * e);
                    }
                    //ditinguish call and put option,then calculate option price of each trial
                    double i_price = 0;
                    if (RadioButton_Call.Checked)
                    {
                        i_price = Math.Max(0, S[i, N] - K);
                    }
                    else
                    {
                        i_price = Math.Max(0, K - S[i, N]);
                    }
                    sum_price = sum_price + i_price;
                    sum_price_2 = sum_price_2 + Math.Pow(i_price, 2);
                }
            }

            //calculate the final price by averaging M trials
            double price = (sum_price / M) * Math.Exp(-r * T);
            //calculate standard error
            double SD = Math.Sqrt((sum_price_2 - (sum_price * sum_price) / M) * Math.Exp(-2 * r * T) / (M - 1));
            double SE = SD / Math.Sqrt(M);

            //set up a vector to store price and standard error
            double[] option = new double[2];
            option[0] = price;
            option[1] = SE;
            //output price and standard error
            return option;
        }

        //calculate Greek values

        private double Delta(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {

            double underlying_plus = European(K, S0 + 0.001 * S0, r, T, sigma, N, M,random)[0];
            double underlying_minus = European(K, S0 - 0.001 * S0, r, T, sigma, N, M,random)[0];
            double delta = (underlying_plus - underlying_minus)/ (0.002 * S0);
            return delta;
        }

        private double Gamma(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double underlying_plus = European(K, S0 + 0.001 * S0, r, T, sigma, N, M,random)[0];
            double underlying_minus = European(K, S0 - 0.001 * S0, r, T, sigma, N, M,random)[0];
            double underlying_mid = European(K, S0, r, T, sigma, N, M,random)[0];
            double gamma = (underlying_plus - 2 * underlying_mid + underlying_minus)/ Math.Pow(1.0*0.001 * S0, 2);
            return gamma;
        }

        private double Vega(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double sigma_plus = European(K, S0, r, T, sigma + 0.001*sigma, N, M,random)[0];
            double sigma_minus = European(K, S0, r, T, sigma - 0.001*sigma, N, M,random)[0];
            double vega = (sigma_plus - sigma_minus)/ (0.002*sigma);
            return vega;
        }

        private double Theta(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double t_plus = European(K, S0, r, T + 0.001*T, sigma, N, M,random)[0];
            double t_mid = European(K, S0, r, T, sigma, N, M,random)[0];
            double theta = -(t_plus - t_mid)/ (0.001*T);
            return theta;
        }

        private double Rho(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double rho_plus = European(K, S0, r + 0.001 * r, T, sigma, N, M,random)[0];
            double rho_minus = European(K, S0, r - 0.001 * r, T, sigma, N, M,random)[0];
            double rho = (rho_plus - rho_minus)/ (0.002 * r);
            return rho;
        }

    }
}
