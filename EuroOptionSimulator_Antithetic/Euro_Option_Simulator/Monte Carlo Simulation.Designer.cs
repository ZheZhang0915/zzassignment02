﻿namespace Euro_Option_Simulator
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox_Underlying = new System.Windows.Forms.TextBox();
            this.label_underlying = new System.Windows.Forms.Label();
            this.RadioButton_Call = new System.Windows.Forms.RadioButton();
            this.Button_Calculate = new System.Windows.Forms.Button();
            this.TextBox_Strike = new System.Windows.Forms.TextBox();
            this.label_Strike = new System.Windows.Forms.Label();
            this.TextBox_Rate = new System.Windows.Forms.TextBox();
            this.label_Rate = new System.Windows.Forms.Label();
            this.TextBox_Volatility = new System.Windows.Forms.TextBox();
            this.label_Volatility = new System.Windows.Forms.Label();
            this.TextBox_Tenor = new System.Windows.Forms.TextBox();
            this.label_Tenor = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBox_Price = new System.Windows.Forms.TextBox();
            this.TextBox_Delta = new System.Windows.Forms.TextBox();
            this.TextBox_Gamma = new System.Windows.Forms.TextBox();
            this.TextBox_Vega = new System.Windows.Forms.TextBox();
            this.TextBox_Theta = new System.Windows.Forms.TextBox();
            this.label_Price = new System.Windows.Forms.Label();
            this.label_Delta = new System.Windows.Forms.Label();
            this.label_Gamma = new System.Windows.Forms.Label();
            this.label_Vega = new System.Windows.Forms.Label();
            this.label_Theta = new System.Windows.Forms.Label();
            this.TextBox_Rho = new System.Windows.Forms.TextBox();
            this.TextBox_Error = new System.Windows.Forms.TextBox();
            this.label_Rho = new System.Windows.Forms.Label();
            this.label_Error = new System.Windows.Forms.Label();
            this.RadioButton_Put = new System.Windows.Forms.RadioButton();
            this.TextBox_Step = new System.Windows.Forms.TextBox();
            this.TextBox_Trial = new System.Windows.Forms.TextBox();
            this.label_Step = new System.Windows.Forms.Label();
            this.label_Trial = new System.Windows.Forms.Label();
            this.RadioButton_Use = new System.Windows.Forms.RadioButton();
            this.RadioButton_Not_use = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBox_Underlying
            // 
            this.TextBox_Underlying.Location = new System.Drawing.Point(151, 36);
            this.TextBox_Underlying.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Underlying.Name = "TextBox_Underlying";
            this.TextBox_Underlying.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Underlying.TabIndex = 0;
            this.TextBox_Underlying.Text = "50\r\n";
            // 
            // label_underlying
            // 
            this.label_underlying.AutoSize = true;
            this.label_underlying.Location = new System.Drawing.Point(58, 39);
            this.label_underlying.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_underlying.Name = "label_underlying";
            this.label_underlying.Size = new System.Drawing.Size(65, 12);
            this.label_underlying.TabIndex = 1;
            this.label_underlying.Text = "Underlying";
            // 
            // RadioButton_Call
            // 
            this.RadioButton_Call.AutoSize = true;
            this.RadioButton_Call.Checked = true;
            this.RadioButton_Call.Location = new System.Drawing.Point(17, 13);
            this.RadioButton_Call.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton_Call.Name = "RadioButton_Call";
            this.RadioButton_Call.Size = new System.Drawing.Size(47, 16);
            this.RadioButton_Call.TabIndex = 2;
            this.RadioButton_Call.TabStop = true;
            this.RadioButton_Call.Text = "Call";
            this.RadioButton_Call.UseVisualStyleBackColor = true;
            // 
            // Button_Calculate
            // 
            this.Button_Calculate.Location = new System.Drawing.Point(571, 86);
            this.Button_Calculate.Margin = new System.Windows.Forms.Padding(2);
            this.Button_Calculate.Name = "Button_Calculate";
            this.Button_Calculate.Size = new System.Drawing.Size(129, 85);
            this.Button_Calculate.TabIndex = 3;
            this.Button_Calculate.Text = "Calculate";
            this.Button_Calculate.UseVisualStyleBackColor = true;
            this.Button_Calculate.Click += new System.EventHandler(this.button1_Click);
            // 
            // TextBox_Strike
            // 
            this.TextBox_Strike.Location = new System.Drawing.Point(151, 66);
            this.TextBox_Strike.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Strike.Name = "TextBox_Strike";
            this.TextBox_Strike.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Strike.TabIndex = 4;
            this.TextBox_Strike.Text = "50";
            // 
            // label_Strike
            // 
            this.label_Strike.AutoSize = true;
            this.label_Strike.Location = new System.Drawing.Point(46, 69);
            this.label_Strike.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Strike.Name = "label_Strike";
            this.label_Strike.Size = new System.Drawing.Size(77, 12);
            this.label_Strike.TabIndex = 5;
            this.label_Strike.Text = "Strike price";
            // 
            // TextBox_Rate
            // 
            this.TextBox_Rate.Location = new System.Drawing.Point(151, 96);
            this.TextBox_Rate.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Rate.Name = "TextBox_Rate";
            this.TextBox_Rate.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Rate.TabIndex = 6;
            this.TextBox_Rate.Text = "0.05";
            // 
            // label_Rate
            // 
            this.label_Rate.AutoSize = true;
            this.label_Rate.Location = new System.Drawing.Point(46, 99);
            this.label_Rate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Rate.Name = "label_Rate";
            this.label_Rate.Size = new System.Drawing.Size(89, 12);
            this.label_Rate.TabIndex = 7;
            this.label_Rate.Text = "Risk-free rate";
            // 
            // TextBox_Volatility
            // 
            this.TextBox_Volatility.Location = new System.Drawing.Point(151, 130);
            this.TextBox_Volatility.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Volatility.Name = "TextBox_Volatility";
            this.TextBox_Volatility.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Volatility.TabIndex = 8;
            this.TextBox_Volatility.Text = "0.5";
            // 
            // label_Volatility
            // 
            this.label_Volatility.AutoSize = true;
            this.label_Volatility.Location = new System.Drawing.Point(58, 133);
            this.label_Volatility.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Volatility.Name = "label_Volatility";
            this.label_Volatility.Size = new System.Drawing.Size(65, 12);
            this.label_Volatility.TabIndex = 9;
            this.label_Volatility.Text = "Volatility";
            // 
            // TextBox_Tenor
            // 
            this.TextBox_Tenor.Location = new System.Drawing.Point(151, 159);
            this.TextBox_Tenor.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Tenor.Name = "TextBox_Tenor";
            this.TextBox_Tenor.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Tenor.TabIndex = 10;
            this.TextBox_Tenor.Text = "1";
            // 
            // label_Tenor
            // 
            this.label_Tenor.AutoSize = true;
            this.label_Tenor.Location = new System.Drawing.Point(71, 159);
            this.label_Tenor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Tenor.Name = "label_Tenor";
            this.label_Tenor.Size = new System.Drawing.Size(35, 12);
            this.label_Tenor.TabIndex = 11;
            this.label_Tenor.Text = "Tenor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(138, -2);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(477, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Monte Carlo Simulation with Antithetic Variance Reduction";
            // 
            // TextBox_Price
            // 
            this.TextBox_Price.Location = new System.Drawing.Point(399, 36);
            this.TextBox_Price.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Price.Name = "TextBox_Price";
            this.TextBox_Price.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Price.TabIndex = 13;
            // 
            // TextBox_Delta
            // 
            this.TextBox_Delta.Location = new System.Drawing.Point(399, 66);
            this.TextBox_Delta.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Delta.Name = "TextBox_Delta";
            this.TextBox_Delta.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Delta.TabIndex = 14;
            // 
            // TextBox_Gamma
            // 
            this.TextBox_Gamma.Location = new System.Drawing.Point(399, 96);
            this.TextBox_Gamma.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Gamma.Name = "TextBox_Gamma";
            this.TextBox_Gamma.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Gamma.TabIndex = 15;
            // 
            // TextBox_Vega
            // 
            this.TextBox_Vega.Location = new System.Drawing.Point(399, 127);
            this.TextBox_Vega.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Vega.Name = "TextBox_Vega";
            this.TextBox_Vega.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Vega.TabIndex = 16;
            // 
            // TextBox_Theta
            // 
            this.TextBox_Theta.Location = new System.Drawing.Point(399, 159);
            this.TextBox_Theta.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Theta.Name = "TextBox_Theta";
            this.TextBox_Theta.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Theta.TabIndex = 17;
            // 
            // label_Price
            // 
            this.label_Price.AutoSize = true;
            this.label_Price.Location = new System.Drawing.Point(308, 39);
            this.label_Price.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Price.Name = "label_Price";
            this.label_Price.Size = new System.Drawing.Size(35, 12);
            this.label_Price.TabIndex = 18;
            this.label_Price.Text = "Price";
            // 
            // label_Delta
            // 
            this.label_Delta.AutoSize = true;
            this.label_Delta.Location = new System.Drawing.Point(308, 69);
            this.label_Delta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Delta.Name = "label_Delta";
            this.label_Delta.Size = new System.Drawing.Size(35, 12);
            this.label_Delta.TabIndex = 19;
            this.label_Delta.Text = "Delta";
            // 
            // label_Gamma
            // 
            this.label_Gamma.AutoSize = true;
            this.label_Gamma.Location = new System.Drawing.Point(308, 99);
            this.label_Gamma.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Gamma.Name = "label_Gamma";
            this.label_Gamma.Size = new System.Drawing.Size(35, 12);
            this.label_Gamma.TabIndex = 20;
            this.label_Gamma.Text = "Gamma";
            // 
            // label_Vega
            // 
            this.label_Vega.AutoSize = true;
            this.label_Vega.Location = new System.Drawing.Point(308, 130);
            this.label_Vega.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Vega.Name = "label_Vega";
            this.label_Vega.Size = new System.Drawing.Size(29, 12);
            this.label_Vega.TabIndex = 21;
            this.label_Vega.Text = "Vega";
            // 
            // label_Theta
            // 
            this.label_Theta.AutoSize = true;
            this.label_Theta.Location = new System.Drawing.Point(308, 162);
            this.label_Theta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Theta.Name = "label_Theta";
            this.label_Theta.Size = new System.Drawing.Size(35, 12);
            this.label_Theta.TabIndex = 22;
            this.label_Theta.Text = "Theta";
            // 
            // TextBox_Rho
            // 
            this.TextBox_Rho.Location = new System.Drawing.Point(399, 192);
            this.TextBox_Rho.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Rho.Name = "TextBox_Rho";
            this.TextBox_Rho.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Rho.TabIndex = 23;
            // 
            // TextBox_Error
            // 
            this.TextBox_Error.Location = new System.Drawing.Point(399, 224);
            this.TextBox_Error.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Error.Name = "TextBox_Error";
            this.TextBox_Error.Size = new System.Drawing.Size(125, 21);
            this.TextBox_Error.TabIndex = 24;
            // 
            // label_Rho
            // 
            this.label_Rho.AutoSize = true;
            this.label_Rho.Location = new System.Drawing.Point(308, 192);
            this.label_Rho.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Rho.Name = "label_Rho";
            this.label_Rho.Size = new System.Drawing.Size(23, 12);
            this.label_Rho.TabIndex = 25;
            this.label_Rho.Text = "Rho";
            // 
            // label_Error
            // 
            this.label_Error.AutoSize = true;
            this.label_Error.Location = new System.Drawing.Point(285, 227);
            this.label_Error.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Error.Name = "label_Error";
            this.label_Error.Size = new System.Drawing.Size(89, 12);
            this.label_Error.TabIndex = 26;
            this.label_Error.Text = "Standard error";
            // 
            // RadioButton_Put
            // 
            this.RadioButton_Put.AutoSize = true;
            this.RadioButton_Put.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RadioButton_Put.Location = new System.Drawing.Point(17, 40);
            this.RadioButton_Put.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton_Put.Name = "RadioButton_Put";
            this.RadioButton_Put.Size = new System.Drawing.Size(41, 16);
            this.RadioButton_Put.TabIndex = 27;
            this.RadioButton_Put.Text = "Put";
            this.RadioButton_Put.UseVisualStyleBackColor = true;
            // 
            // TextBox_Step
            // 
            this.TextBox_Step.Location = new System.Drawing.Point(151, 192);
            this.TextBox_Step.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Step.Name = "TextBox_Step";
            this.TextBox_Step.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Step.TabIndex = 28;
            this.TextBox_Step.Text = "100";
            // 
            // TextBox_Trial
            // 
            this.TextBox_Trial.Location = new System.Drawing.Point(151, 224);
            this.TextBox_Trial.Margin = new System.Windows.Forms.Padding(2);
            this.TextBox_Trial.Name = "TextBox_Trial";
            this.TextBox_Trial.Size = new System.Drawing.Size(68, 21);
            this.TextBox_Trial.TabIndex = 29;
            this.TextBox_Trial.Text = "10000";
            // 
            // label_Step
            // 
            this.label_Step.AutoSize = true;
            this.label_Step.Location = new System.Drawing.Point(71, 192);
            this.label_Step.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Step.Name = "label_Step";
            this.label_Step.Size = new System.Drawing.Size(29, 12);
            this.label_Step.TabIndex = 30;
            this.label_Step.Text = "Step";
            // 
            // label_Trial
            // 
            this.label_Trial.AutoSize = true;
            this.label_Trial.Location = new System.Drawing.Point(71, 227);
            this.label_Trial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Trial.Name = "label_Trial";
            this.label_Trial.Size = new System.Drawing.Size(35, 12);
            this.label_Trial.TabIndex = 31;
            this.label_Trial.Text = "Trial";
            // 
            // RadioButton_Use
            // 
            this.RadioButton_Use.AutoSize = true;
            this.RadioButton_Use.Checked = true;
            this.RadioButton_Use.Location = new System.Drawing.Point(2, 13);
            this.RadioButton_Use.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton_Use.Name = "RadioButton_Use";
            this.RadioButton_Use.Size = new System.Drawing.Size(221, 16);
            this.RadioButton_Use.TabIndex = 32;
            this.RadioButton_Use.TabStop = true;
            this.RadioButton_Use.Text = "Use antithetic variance reduction";
            this.RadioButton_Use.UseVisualStyleBackColor = true;
            // 
            // RadioButton_Not_use
            // 
            this.RadioButton_Not_use.AutoSize = true;
            this.RadioButton_Not_use.Location = new System.Drawing.Point(2, 40);
            this.RadioButton_Not_use.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton_Not_use.Name = "RadioButton_Not_use";
            this.RadioButton_Not_use.Size = new System.Drawing.Size(245, 16);
            this.RadioButton_Not_use.TabIndex = 33;
            this.RadioButton_Not_use.Text = "Not use antithetic variance reduction";
            this.RadioButton_Not_use.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadioButton_Call);
            this.panel1.Controls.Add(this.RadioButton_Put);
            this.panel1.Location = new System.Drawing.Point(424, 262);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 75);
            this.panel1.TabIndex = 34;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RadioButton_Use);
            this.panel2.Controls.Add(this.RadioButton_Not_use);
            this.panel2.Location = new System.Drawing.Point(151, 262);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 75);
            this.panel2.TabIndex = 28;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 349);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label_Trial);
            this.Controls.Add(this.label_Step);
            this.Controls.Add(this.TextBox_Trial);
            this.Controls.Add(this.TextBox_Step);
            this.Controls.Add(this.label_Error);
            this.Controls.Add(this.label_Rho);
            this.Controls.Add(this.TextBox_Error);
            this.Controls.Add(this.TextBox_Rho);
            this.Controls.Add(this.label_Theta);
            this.Controls.Add(this.label_Vega);
            this.Controls.Add(this.label_Gamma);
            this.Controls.Add(this.label_Delta);
            this.Controls.Add(this.label_Price);
            this.Controls.Add(this.TextBox_Theta);
            this.Controls.Add(this.TextBox_Vega);
            this.Controls.Add(this.TextBox_Gamma);
            this.Controls.Add(this.TextBox_Delta);
            this.Controls.Add(this.TextBox_Price);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label_Tenor);
            this.Controls.Add(this.TextBox_Tenor);
            this.Controls.Add(this.label_Volatility);
            this.Controls.Add(this.TextBox_Volatility);
            this.Controls.Add(this.label_Rate);
            this.Controls.Add(this.TextBox_Rate);
            this.Controls.Add(this.label_Strike);
            this.Controls.Add(this.TextBox_Strike);
            this.Controls.Add(this.Button_Calculate);
            this.Controls.Add(this.label_underlying);
            this.Controls.Add(this.TextBox_Underlying);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Monte Carlo Simulator";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox_Underlying;
        private System.Windows.Forms.Label label_underlying;
        private System.Windows.Forms.RadioButton RadioButton_Call;
        private System.Windows.Forms.Button Button_Calculate;
        private System.Windows.Forms.TextBox TextBox_Strike;
        private System.Windows.Forms.Label label_Strike;
        private System.Windows.Forms.TextBox TextBox_Rate;
        private System.Windows.Forms.Label label_Rate;
        private System.Windows.Forms.TextBox TextBox_Volatility;
        private System.Windows.Forms.Label label_Volatility;
        private System.Windows.Forms.TextBox TextBox_Tenor;
        private System.Windows.Forms.Label label_Tenor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBox_Price;
        private System.Windows.Forms.TextBox TextBox_Delta;
        private System.Windows.Forms.TextBox TextBox_Gamma;
        private System.Windows.Forms.TextBox TextBox_Vega;
        private System.Windows.Forms.TextBox TextBox_Theta;
        private System.Windows.Forms.Label label_Price;
        private System.Windows.Forms.Label label_Delta;
        private System.Windows.Forms.Label label_Gamma;
        private System.Windows.Forms.Label label_Vega;
        private System.Windows.Forms.Label label_Theta;
        private System.Windows.Forms.TextBox TextBox_Rho;
        private System.Windows.Forms.TextBox TextBox_Error;
        private System.Windows.Forms.Label label_Rho;
        private System.Windows.Forms.Label label_Error;
        private System.Windows.Forms.RadioButton RadioButton_Put;
        private System.Windows.Forms.TextBox TextBox_Step;
        private System.Windows.Forms.TextBox TextBox_Trial;
        private System.Windows.Forms.Label label_Step;
        private System.Windows.Forms.Label label_Trial;
        private System.Windows.Forms.RadioButton RadioButton_Use;
        private System.Windows.Forms.RadioButton RadioButton_Not_use;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}

