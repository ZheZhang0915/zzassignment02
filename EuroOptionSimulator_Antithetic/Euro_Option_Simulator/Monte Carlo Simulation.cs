﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Euro_Option_Simulator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            try
            {

                //read inputs from textboxes
                double S0 = Convert.ToDouble(TextBox_Underlying.Text);
                double K = Convert.ToDouble(TextBox_Strike.Text);
                double r = Convert.ToDouble(TextBox_Rate.Text);
                double sigma = Convert.ToDouble(TextBox_Volatility.Text);
                double T = Convert.ToDouble(TextBox_Tenor.Text);
                int N = Convert.ToInt32(TextBox_Step.Text);
                int M = Convert.ToInt32(TextBox_Trial.Text);

                if (S0 > 0 && K > 0 && r > 0 && sigma > 0 && T > 0 && N > 1 && M > 0)
                {
                    //output results through form text                   
                        double[,] random = GetRandom(N, M);
                        TextBox_Price.Text = Convert.ToString(European(K, S0, r, T, sigma, N, M, random)[0]);
                        TextBox_Error.Text = Convert.ToString(European(K, S0, r, T, sigma, N, M, random)[1]);
                        TextBox_Delta.Text = Convert.ToString(Delta(K, S0, r, T, sigma, N, M, random));
                        TextBox_Gamma.Text = Convert.ToString(Gamma(K, S0, r, T, sigma, N, M, random));
                        TextBox_Vega.Text = Convert.ToString(Vega(K, S0, r, T, sigma, N, M, random));
                        TextBox_Theta.Text = Convert.ToString(Theta(K, S0, r, T, sigma, N, M, random));
                        TextBox_Rho.Text = Convert.ToString(Rho(K, S0, r, T, sigma, N, M, random));                                       
                }

                else
                {
                    MessageBox.Show("Please input positive number in every leftward textbox ans step should be greater than 1.");
                }
            }
            catch
            {
                MessageBox.Show("Please input positive number in every leftward textbox and step should be greater than 1.");
            }

        }
        
        private double[,] GetRandom(int N, int M)
        {
            //set up a matrix to store random numbers
            double[,] random = new double[M, N + 1];
            Random rnd = new Random();
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j <= N; j++)
                //generate random numbers using polar rejection method
                {
                    double x1 = 0;
                    double x2 = 0;
                    double w = 2;
                    while (w > 1)
                    {
                        x1 = rnd.NextDouble() * 2 - 1;
                        x2 = rnd.NextDouble() * 2 - 1;
                        w = Math.Pow(x1, 2) + Math.Pow(x2, 2);
                    }
                    double c = 0;
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    random[i, j] = 0;
                    random[i, j] = c * x1;
                }
            }
            return random;
        } 

        private double[] European(double K, double S0, double r, double T, double sigma, int N, int M,double[,] random)
        { 
            

            //calculate some inputs of simulation formular
            double dt = T / N;
            double nudt = (r - 0.5 * Math.Pow(sigma, 2)) * dt;
            double sum_price = 0;
            double sum_price_2 = 0;

            //when we choose to use antithetic variance reduction
            if (RadioButton_Not_use.Checked)
            {
                //set up a matrix to store St
                double[,] S = new double[M, N + 1];
                for (int i = 0; i < M; i++)
                {
                    S[i, 0] = S0;
                }

                //calculate St under trial i and step j
                for (int i = 0; i < M; i++)
                {
                    for (int j = 1; j <= N; j++)
                    {
                        double e = random[i, j];
                        S[i, j] = S[i, j - 1] * Math.Exp(nudt + sigma * Math.Sqrt(dt) * e);
                    }
                    //ditinguish call and put option,then calculate option price of each trial
                    double i_price = 0;
                    if (RadioButton_Call.Checked)
                    {
                        i_price = Math.Max(0, S[i, N] - K);
                    }
                    else
                    {
                        i_price = Math.Max(0, K - S[i, N]);
                    }
                    sum_price = sum_price + i_price;
                    sum_price_2 = sum_price_2 + Math.Pow(i_price, 2);
                }
            }
            //when we do not choose to use antithetic variance reduction
            else
            {
                //set up a matrix to store St
                double[,] S = new double[2*M, N + 1];
                for (int i = 0; i < 2*M; i++)
                {
                    S[i, 0] = S0;
                }

                //calculate St under trial i and step j
                for (int i = 0; i < M; i++)
                {
                    for (int j = 1; j <= N; j++)
                    {
                        double e = random[i, j];
                        S[i, j] = S[i, j - 1] * Math.Exp(nudt + sigma * Math.Sqrt(dt) * e);
                        S[i + M, j] = S[i + M, j - 1] * Math.Exp(nudt + sigma * Math.Sqrt(dt) * (-e));
                    }
                    //ditinguish call and put option,then calculate option price of each trial
                    double i_price = 0;
                    if (RadioButton_Call.Checked)
                    {
                        i_price = 0.5 * (Math.Max(0, S[i, N] - K) + Math.Max(0, S[i + M, N] - K));
                    }
                    else
                    {
                        i_price = 0.5 * (Math.Max(0, K - S[i, N]) + Math.Max(0, K - S[i + M, N]));
                    }
                    sum_price = sum_price + i_price;
                    sum_price_2 = sum_price_2 + Math.Pow(i_price, 2);
                }

            }

            //calculate the final price by averaging M trials
            double price = (sum_price / M) * Math.Exp(-r * T);
            //calculate standard error
            double SD = Math.Sqrt((sum_price_2 - (sum_price * sum_price) / M) * Math.Exp(-2 * r * T) / (M - 1));
            double SE = SD / Math.Sqrt(M);

            //set up a vector to store price and standard error
            double[] option = new double[2];
            option[0] = price;
            option[1] = SE;
            //output price and standard error
            return option;
        }

        //calculate Greek values

        private double Delta(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {

            double underlying_plus = European(K, S0 + 0.001 * S0, r, T, sigma, N, M,random)[0];
            double underlying_minus = European(K, S0 - 0.001 * S0, r, T, sigma, N, M,random)[0];
            double delta = (underlying_plus - underlying_minus)/ (0.002 * S0);
            return delta;
        }

        private double Gamma(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double underlying_plus = European(K, S0 + 0.001 * S0, r, T, sigma, N, M,random)[0];
            double underlying_minus = European(K, S0 - 0.001 * S0, r, T, sigma, N, M,random)[0];
            double underlying_mid = European(K, S0, r, T, sigma, N, M,random)[0];
            double gamma = (underlying_plus - 2 * underlying_mid + underlying_minus)/ Math.Pow(1.0*0.001 * S0, 2);
            return gamma;
        }

        private double Vega(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double sigma_plus = European(K, S0, r, T, sigma + 0.001*sigma, N, M,random)[0];
            double sigma_minus = European(K, S0, r, T, sigma - 0.001*sigma, N, M,random)[0];
            double vega = (sigma_plus - sigma_minus)/ (0.002*sigma);
            return vega;
        }

        private double Theta(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double t_plus = European(K, S0, r, T + 0.001*T, sigma, N, M,random)[0];
            double t_mid = European(K, S0, r, T, sigma, N, M,random)[0];
            double theta = -(t_plus - t_mid)/ (0.001*T);
            return theta;
        }

        private double Rho(double K, double S0, double r, double T, double sigma, int N, int M, double[,] random)
        {
            double rho_plus = European(K, S0, r + 0.001 * r, T, sigma, N, M,random)[0];
            double rho_minus = European(K, S0, r - 0.001 * r, T, sigma, N, M,random)[0];
            double rho = (rho_plus - rho_minus)/ (0.002 * r);
            return rho;
        }

        
    }
}
